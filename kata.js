const R = Symbol();
const D = Symbol();
const L = Symbol();
const U = Symbol();

function createSpiral(N) {
    if(!Number.isInteger(N))
        return [];
    let a = emptySpiral(N);
    let d = R;
    let i = 1;
    let s = N;
    let r = 0;
    let c = 0;
    for (let n = 1; n <= N * N; n++) {
        if (i > s) {
            i = 1;
            switch (d) {
                case R:
                    d = D;
                    s--;
                    r++;
                    break;
                case D:
                    d = L;
                    c--;
                    break;
                case L:
                    d = U;
                    s--;
                    r--;
                    break;
                case U:
                    d = R;
                    c++;
                    break;
            }
        }
        a[r][c] = n;
        i++;
        if (i <= s) {
            switch (d) {
                case R:
                    c++;
                    break;
                case D:
                    r++;
                    break;
                case L:
                    c--;
                    break;
                case U:
                    r--;
                    break;
            }
        }
    }
    return spiralToArray(a);
}

function emptySpiral(N) {
    let a = [];
    for (let i = 1; i <= N; i++) a.push(i);
    return a.reduce((pV, cV, cI) => {
        pV[cI] = Array(N).fill(NaN)
        return pV;
    }, {size: N});
}

function spiralToArray(spiral) {
    let res = [];
    for (let i = 0; i < spiral.size; i++)
        res.push(spiral[i]);
    return res;
}

console.log(createSpiral(0));
console.log(createSpiral(1));
console.log(createSpiral(2));
console.log(createSpiral(3));
console.log(createSpiral(4));
console.log(createSpiral(5));
console.log(createSpiral(4.5));